# SPH
Έγινε ένας μικρός καθαρισμός ώστε να μην σας μπερδέψει τίποτα  που είναι ασχέτο με την προσομοίωση αυτή καθαυτή.

Το πήρα από εδώ: https://github.com/leonardo-montes/Unity-ECS-Job-System-SPH

Έχει και ένα σχετικό άρθρο, το οποίο έχει περισσότερο σχέση με το πως θα μπορούσαμε να την κάνουμε πιο efficient στην Unity παρά με την θεωρία. Το βάζω εδώ μην τυχόν κάποιος θέλει να το τσεκάρει:
https://medium.com/@leomontes_60748/how-to-implement-a-fluid-simulation-on-the-cpu-with-unity-ecs-job-system-bf90a0f2724f

Πειραματίστηκα και λίγο να δω πως παίζουν και ανέβασα 2 βιντεάκια μήπως σας βοηθήσουν:
[Basic](https://youtu.be/MQjVJDRJbRw) , [Better_Performance](https://youtu.be/zu0_tyLwxoI)
